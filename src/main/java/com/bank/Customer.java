package com.bank;

public class Customer {

	private String id;
	private String name;
	private String address;
	private String balance;
	
	public Customer(String id, String name, String address, String balance) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.balance = balance;
	}
	
	public Customer( String name, String address, String balance) {
		this.name = name;
		this.address = address;
		this.balance = balance;
	}

	public Customer( String name, String address) {
		this.name = name;
		this.address = address;
		this.balance = null;
	}
	
	public Customer() {
		this.id = null;
		this.name = null;
		this.address = null;
		this.balance = null;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}
	
	
	
	
}
