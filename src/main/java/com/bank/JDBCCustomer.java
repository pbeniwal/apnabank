package com.bank;

//STEP 1. Import required packages
import java.sql.*;

public class JDBCCustomer {
 // JDBC driver name and database URL
 static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";  
 static final String DB_URL = "jdbc:mysql://mysqldb:3306/apnabank";

 //  Database credentials
 static final String USER = "root";
 static final String PASS = "root";
 
 public String InsertRecord(Customer customer) {
	 
 Connection conn = null;
 Statement stmt = null;
 String myid = null;
 
 try{
    //STEP 2: Register JDBC driver
    Class.forName("com.mysql.jdbc.Driver");

    //STEP 3: Open a connection
    System.out.println("Connecting to a selected database...");
    System.out.println(DB_URL);
    conn = DriverManager.getConnection(DB_URL, USER, PASS);
    System.out.println("Connected database successfully...");
    
    //STEP 4: Execute a query
    System.out.println("Inserting records into the table...");
    stmt = conn.createStatement();
    
    PreparedStatement pstmt = null;
    
    String sql = "INSERT INTO account (name,address,balance) " +
                 "VALUES ( ?,?,?)";
    
    pstmt = conn.prepareStatement(sql);
    pstmt.setString(1, customer.getName());
    pstmt.setString(2, customer.getAddress());
    
    if (customer.getBalance()== null) {
    	pstmt.setFloat(3, (float) 0.0);
    }
    else {
        pstmt.setFloat(3, Float.parseFloat(customer.getBalance()));//(3, Float.parseFloat(customer.getBalance()));
    }
    
    pstmt.execute();
    
    System.out.println("Inserted records into the table...");

    ResultSet rs;
    rs = stmt.executeQuery("select max(id) from account");
    while (rs.next()) {
    	myid = rs.getString(1);
    }
    
 }catch(SQLException se){
    //Handle errors for JDBC
    se.printStackTrace();
 }catch(Exception e){
    //Handle errors for Class.forName
    e.printStackTrace();
 }finally{
    //finally block used to close resources
    try{
       if(stmt!=null)
          conn.close();
    }catch(SQLException se){
    }// do nothing
    try{
       if(conn!=null)
          conn.close();
    }catch(SQLException se){
       se.printStackTrace();
    }//end finally try
 }//end try

 System.out.println("Goodbye!");
 return myid;
}

public Customer FetchRecord(String id) {
	
	 Connection conn = null;
	 Statement stmt = null;
	 ResultSet rs;
	 Customer cust = new Customer();
	 
	 try{
	    //STEP 2: Register JDBC driver
	    Class.forName("com.mysql.jdbc.Driver");

	    //STEP 3: Open a connection
	    System.out.println("Connecting to a selected database...");
	    conn = DriverManager.getConnection(DB_URL, USER, PASS);
	    System.out.println("Connected database successfully...");
	    
	    //STEP 4: Execute a query
	    System.out.println("Selecting record from table for id..." + id);
	    stmt = conn.createStatement();
	    
	    PreparedStatement pstmt = null;
	    
	    String sql = "SELECT * FROM account where " +
	                 "ID =  ?";
	    
	    pstmt = conn.prepareStatement(sql);
	    pstmt.setString(1, id);
	   
	   // pstmt.setString(2, customer.getAddress());
	    //pstmt.setString(3, customer.getPhotoID());
	    
	    //rs = pstmt.getResultSet();
	    
	    rs = pstmt.executeQuery();
	    
	    System.out.println("Selected records from the table...");
	    
	    //System.out.println("id : " + rs.getString("id"));
	    
	    while (rs.next()) {
	   // System.out.println("id : " + rs.getString("id"));
	    	cust.setId(rs.getString(1));
	    	cust.setName(rs.getString(2));
	    	cust.setAddress(rs.getString(3));
	    	cust.setBalance(rs.getString(4));
	    }
	    
	 }catch(SQLException se){
	    //Handle errors for JDBC
	    se.printStackTrace();
	 }catch(Exception e){
	    //Handle errors for Class.forName
	    e.printStackTrace();
	 }finally{
	    //finally block used to close resources
	    try{
	       if(stmt!=null)
	          conn.close();
	    }catch(SQLException se){
	    }// do nothing
	    try{
	       if(conn!=null)
	          conn.close();
	    }catch(SQLException se){
	       se.printStackTrace();
	    }//end finally try
	 }//end try

	 System.out.println("Goodbye!");
	 return cust;
}

public Customer UpdateBalance(String id, String amount) {
	
	 Connection conn = null;
	 Statement stmt = null;
	 ResultSet rs;
	 Customer cust = new Customer();
	 float amt;
	 
	 cust = this.FetchRecord(id);
	 
	 amt = Float.parseFloat(cust.getBalance()) + Float.parseFloat(amount);
	 
	 cust.setBalance(String.valueOf(amt));
	 
	 try{
		    //STEP 2: Register JDBC driver
		    Class.forName("com.mysql.jdbc.Driver");

		    //STEP 3: Open a connection
		    System.out.println("Connecting to a selected database...");
		    conn = DriverManager.getConnection(DB_URL, USER, PASS);
		    System.out.println("Connected database successfully...");
		    
		    //STEP 4: Execute a query
		    System.out.println("Selecting record from table for id..." + id);
		    stmt = conn.createStatement();
		    
		    PreparedStatement pstmt = null;
		    
		    String sql = "UPDATE account set BALANCE = ?  where " +
		                 "ID =  ?";
		    
		    pstmt = conn.prepareStatement(sql);
		    pstmt.setString(2, id);
		    pstmt.setFloat(1, amt);
		   
		   // pstmt.setString(2, customer.getAddress());
		    //pstmt.setString(3, customer.getPhotoID());
		    
		    //rs = pstmt.getResultSet();
		    
		    int ret  = pstmt.executeUpdate();
		    
		    
		    System.out.println("Selected records from the table...");
		    
		    //System.out.println("id : " + rs.getString("id"));
		    
		    //while (rs.next()) {
		   // System.out.println("id : " + rs.getString("id"));
		 //   	cust.setId(rs.getString(1));
		 //   	cust.setName(rs.getString(2));
		 //   	cust.setAddress(rs.getString(3));
		 //   	cust.setBalance(rs.getString(4));
		 //   }
		    
		 }catch(SQLException se){
		    //Handle errors for JDBC
		    se.printStackTrace();
		 }catch(Exception e){
		    //Handle errors for Class.forName
		    e.printStackTrace();
		 }finally{
		    //finally block used to close resources
		    try{
		       if(stmt!=null)
		          conn.close();
		    }catch(SQLException se){
		    }// do nothing
		    try{
		       if(conn!=null)
		          conn.close();
		    }catch(SQLException se){
		       se.printStackTrace();
		    }//end finally try
		 }//end try

		 System.out.println("Goodbye!");
		 
	 return cust;
}
}
