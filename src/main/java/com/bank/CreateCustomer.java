package com.bank;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CreateCustomer extends  HttpServlet{
	
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response ) {		

		String name = request.getParameter("name");;
		String address = request.getParameter("address");
		//String photoID = request.getParameter("photoid");
		
		Customer cust = new Customer(name, address);
		
		String id = (new JDBCCustomer()).InsertRecord(cust);
		
		try {
			PrintWriter out = response.getWriter();
			out.println("Customer created successfully with id " + id);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
