package com.bank;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Deposit extends  HttpServlet{
	
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response ) {		

		String id = request.getParameter("id");;
		String amount = request.getParameter("amount");
		//String photoID = request.getParameter("photoid");
		
		Customer cust = new Customer();
		
		//id="3";
		cust = (new JDBCCustomer()).UpdateBalance(id,amount);
		
		try {
			PrintWriter out = response.getWriter();
			out.println("Account Balance with id " + id + " successfully updated to " +cust.getBalance());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
