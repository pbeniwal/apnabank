package com.bank;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FetchCustomer extends  HttpServlet{
	
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response ) {		

		String id = request.getParameter("id");
		
		Customer cust = (new JDBCCustomer()).FetchRecord(id);
		
		System.out.println(cust.getName());
		System.out.println(cust.getAddress());
		System.out.println(cust.getBalance());
		
		try {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();			
			out.println("<html>");
		    out.println("<head>");
		    out.println("<title>Customer Details</title>");
		    out.println("</head>");
		    out.println("<body bgcolor=\"white\">");		    
			out.println("Customer details:<br>");
			out.println("id:  " + cust.getId()); 
			out.println("<br>Name: " + cust.getName());
			out.println("<br>Address: " + cust.getAddress());
			out.println("<br> Balance: " + cust.getBalance());						
			out.println("<br><br> <form action=\"deposit.jsp\">");
			out.println("<input type=\"submit\" value=\"Deposit\">");
			out.println("</form>");			
			out.println("<br><br><br> <form action=\"index.jsp\">"); 
			out.println("<input type=\"submit\" value=\"Back to Main Page\">");
			out.println("</form>");		
		    out.println("</body>");
		    out.println("</html>");
		    
			/*
		    out.println("<html>");
		    out.println("<head>");
		    out.println("<title>Hola</title>");
		    out.println("</head>");
		    out.println("<body bgcolor=\"white\">");
		    out.println("</body>");
		    out.println("</html>");*/
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
