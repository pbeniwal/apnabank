package com.bank;

import static org.junit.Assert.*;
import java.io.*;
import javax.servlet.http.*;
//import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.mockito.Mockito;

public class testCustomer extends Mockito{

    @Test
    public void testServlet() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);    

        when(request.getParameter("name")).thenReturn("junit mockito");
        when(request.getParameter("address")).thenReturn("Pune");
        //when(request.getParameter("balance")).thenReturn("12345");

        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        
        System.out.println("1 stringWriter is : " + stringWriter.toString());
        
        when(response.getWriter()).thenReturn(writer);
        
        System.out.println("2 stringWriter is : " + stringWriter.toString());

        new CreateCustomer().doPost(request, response);

        //verify(request, atLeast(1)).getParameter("name"); // only if you want to verify username was called...
        writer.flush(); // it may not have been flushed yet...
        
        System.out.println("3 stringWriter is : " + stringWriter.toString());
        
        assertTrue(stringWriter.toString().contains("Customer created successfully with id"));
    }
}